#pragma once
#include "Piece.h"

class Bishop : public Piece
{
public:
	Bishop(int color);
	virtual int checkMove(int From[2], int To[2], Piece* board[][8]) const;
	virtual char getType() const;
};
