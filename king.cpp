#include "king.h"

King::King(int color) :Piece(color)
{
}

int King::checkMove(int From[2], int To[2], Piece * board[][8]) const
{
	if (abs(From[0] - To[0]) > 1 || abs(From[1] - To[1]) > 1)
	{
		return 0;
	}
	return 1;
}

char King::getType() const
{
	if (this->color)
	{
		return 'k';
	}
	return 'K';
}
