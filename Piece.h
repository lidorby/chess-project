#pragma once
#include <iostream>
#include <string>
class Piece
{
protected:
	int color;//0-black,1-white
public:
	Piece(int color);
	virtual int checkMove(int From[2], int To[2], Piece* board[][8]) const = 0;//this func checks if the move is valid for each piece. returns 1 if is good move,0 if not
	virtual int getColor();//returns the color-> 1-white,0-black
	virtual char getType() const = 0;//returns the type of each piece->1-small letter,2-big letter
};