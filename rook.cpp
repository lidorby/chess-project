#include "rook.h"

Rook::Rook(int color): Piece(color)
{
}

int Rook::checkMove(int From[2], int To[2],Piece* board[][8]) const
{
	int i = 0;
	int j = 0;
	if (From[0] != To[0] && From[1] != To[1]) {
		return 0;//this piece cant move this Toition
	}
	else if (From[0] != To[0] && abs(From[0] - To[0]) > 1) {//check if we move on the y of the board
		if (From[0] < To[0]) {
			for (i = From[0] + 1; i < To[0]; i++) {
				if (board[i][From[1]]) {
					return 0;
				}
			}
		}
		else {
			for (i = From[0] - 1; i > To[0]; i--) {
				if (board[i][From[1]]) {
					return 0;
				}
			}
		}
	}
	else if (From[1] != To[1] && abs(From[1] - To[1]) > 1) {//check if we move on the x
		if (From[1] < To[1]) {
			for (i = From[1] + 1; i < To[1]; i++) {
				if (board[From[0]][i]) {
					return 0;
				}
			}
		}
		else {
			for (i = From[1] - 1; i > To[1]; i--) {
				if (board[From[0]][i]) {
					return 0;
				}
			}
		}
	}
	//if there was no problem with the mvoe it will return 1(valid move)
	return 1;
}

char Rook::getType() const
{
	if (this->color)
	{
		return 'r';
	}
	return 'R';
}
