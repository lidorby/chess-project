#include "pawn.h"

Pawn::Pawn(int color):Piece(color)
{
}

int Pawn::checkMove(int From[2], int To[2], Piece * board[][8]) const
{
	bool Moved = 1;
	if (this->color == 0)//black
	{
		if (From[0] == 1)
		{
			Moved = 0;
		}
		if((abs(To[1] - From[1]) == 1) & board[To[0]][To[1]] != 0)//he tries to eat someone
		{
			if (To[0] - From[0] == 1)//regular move
			{
				return 1;
			}

		}
		else if (To[1] - From[1] != 0)// if he tries to move to the sides
		{
			return 0;
		}
		if (!Moved)//if he havent moved yet
		{
				if (To[0] - From[0] == 2)
				{
					return 1;
				}
		}
		if (To[0] - From[0] == 1)//regular move
		{
			return 1;
		}
		return 0;
	}
	else//white
	{
		if (From[0] == 6)
		{
			Moved = 0;
		}
		if ((abs(To[1] - From[1]) == 1) & board[To[0]][To[1]] != 0)//he tries to eat someone
		{

			if (To[0] - From[0] == -1)//regular move
			{
				return 1;
			}

		}
		else if (To[1] - From[1] != 0)// if he tries to move to the sides
		{
			return 0;
		}
		if (!Moved)//if he havent moved yet
		{
			if (To[0] - From[0] == -2)
			{
				return 1;
			}
		}

		if (To[0] - From[0] == -1)//regular move
		{
			return 1;
		}
	}
		return 0;



	return 0;
}

char Pawn::getType() const
{
	if (this->color)
	{
		return 'p';
	}
	return 'P';
}
