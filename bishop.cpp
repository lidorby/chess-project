#include "bishop.h"

int Bishop::checkMove(int From[2], int To[2], Piece* board[][8]) const
{
	int i = 0;
	int j = 0;
	if (abs(From[0] - To[0]) != abs(From[1] - To[1])) {
		return 0;//this piece cant move this Toition
	}
	else if (abs(From[0] - To[0]) > 1)//checks if there is a piece in the way
	{
		if (From[0] < To[0])
		{
			if (From[1] < To[1])
			{
				for (i = From[0] + 1, j = From[1] + 1; i < To[0], j < To[1]; i++, j++)
				{
					if (board[i][j])
					{
						return 0;
					}
				}
			}
			else {
				for (i = From[0] + 1, j = From[1] - 1; i < To[0], j > To[1]; i++, j--)
				{
					if (board[i][j])
					{
						return 0;
					}
				}
			}
		}
		else {
			if (From[1] > To[1])
			{
				for (i = From[0] - 1, j = From[1] - 1; i > To[0], j > To[1]; i--, j--)
				{
					if (board[i][j])
					{
						return 0;
					}
				}
			}
			else {
				for (i = From[0] - 1, j = From[1] + 1; i > To[0], j < To[1]; i--, j++)
				{
					if (board[i][j])
					{
						return 0;
					}
				}
			}
		}
	}

	return 1;
}

Bishop::Bishop(int color):Piece(color)
{
}


char Bishop::getType() const
{
	if (this->color)
	{
		return 'b';
	}
	return 'B';
}
