#pragma once
#include "Piece.h"
#include "rook.h"
#include "bishop.h"
class Queen : public Piece
{
public:
	Queen(int color);
	virtual int checkMove(int From[2], int To[2], Piece* board[][8]) const;
	virtual char getType() const;
};