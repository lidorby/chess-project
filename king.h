#pragma once
#include "Piece.h"

class King : public Piece
{
public:
	King(int color);
	virtual int checkMove(int From[2],int To[2], Piece* board[][8]) const;
	virtual char getType() const;
};

