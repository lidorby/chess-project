#include "board.h"

Board::Board(std::string data)//setting up the board with the data
{
	if (data[64] == '0')//because we did that 1-white, and 0- black
	{
		this->turn = 1;
	}
	else
	{
		this->turn = 0;
	}
	for (int i = 0; i < 8; i++)//setting up the board(meanwhile)
	{
		for (int j = 0; j < 8; j++)
		{
			this->_board[i][j] = 0;
		}
	}
	//black
	this->_board[0][0] = new Rook(0);
	this->_board[0][7] = new Rook(0);
	this->_board[0][2] = new Bishop(0);
	this->_board[0][5] = new Bishop(0);
	this->_board[0][3] = new King(0);
	this->_board[0][4] = new Queen(0);
	this->_board[0][1] = new Knight(0);
	this->_board[0][6] = new Knight(0);
	this->_board[1][0] = new Pawn(0);
	this->_board[1][1] = new Pawn(0);
	this->_board[1][2] = new Pawn(0);
	this->_board[1][3] = new Pawn(0);
	this->_board[1][4] = new Pawn(0);
	this->_board[1][5] = new Pawn(0);
	this->_board[1][6] = new Pawn(0);
	this->_board[1][7] = new Pawn(0);


	//white
	this->_board[7][0] = new Rook(1);
	this->_board[7][7] = new Rook(1);
	this->_board[7][2] = new Bishop(1);
	this->_board[7][5] = new Bishop(1);
	this->_board[7][3] = new King(1);
	this->_board[7][4] = new Queen(1);
	this->_board[7][1] = new Knight(1);
	this->_board[7][6] = new Knight(1);
	this->_board[6][0] = new Pawn(1);
	this->_board[6][1] = new Pawn(1);
	this->_board[6][2] = new Pawn(1);
	this->_board[6][3] = new Pawn(1);
	this->_board[6][4] = new Pawn(1);
	this->_board[6][5] = new Pawn(1);
	this->_board[6][6] = new Pawn(1);
	this->_board[6][7] = new Pawn(1);
}
//getting a locations(from,to) that we got from the chess game
//checking if the move is valid, if not we are returning the correct error
//if yes, we returning 1 if there is a chess, and 0 if its just a regular move
std::string Board::MakeMove(std::string location)
{
	int From[2] = { abs(int(location[1] - 48 - 8)),int(location[0] - 97) };
	int To[2] = { abs(int(location[3] - 48 - 8)),int(location[2] - 97) };
	Piece* Tomove;
	Piece* Moveto;
	std::string firstLocation = location.substr(0, 2);
	std::string secondLocation = location.substr(2);
	Tomove = getPieceInLocation(firstLocation);
	Moveto = getPieceInLocation(secondLocation);
	movePieces(From,To,location);
	if (Tomove == nullptr)// if there is nothing on the object
	{
		this->_board[From[0]][From[1]] = Tomove;
		this->_board[To[0]][To[1]] = Moveto;
		return "2";
	}
	else if ((!turn && Tomove->getColor()) || ((turn && !Tomove->getColor())))//if he trying to use the enemy's piece
	{
		this->_board[From[0]][From[1]] = Tomove;
		this->_board[To[0]][To[1]] = Moveto;
		return "2";
	}	
	else if (Moveto!= 0 && ((!turn && !Moveto->getColor()) || (turn && Moveto->getColor())))//if he is trying to put piece on his piece
	{
		this->_board[From[0]][From[1]] = Tomove;
		this->_board[To[0]][To[1]] = Moveto;
		return "3";
	}
	else if (checkChess(this->turn))//f there would be a self chess 
	{
		this->_board[From[0]][From[1]] = Tomove;
		this->_board[To[0]][To[1]] = Moveto;
		return "4";
	}
	else if (Tomove == nullptr)//if its out of bounds
	{
		this->_board[From[0]][From[1]] = Tomove;
		this->_board[To[0]][To[1]] = Moveto;
		return "5";
	}
	else if (!Tomove->checkMove(From,To,this->_board))//if the move is illegal(cause the piece cant move like that)
	{
		this->_board[From[0]][From[1]] = Tomove;
		this->_board[To[0]][To[1]] = Moveto;
		return "6";
	}
	else if (firstLocation.compare(secondLocation) == 0)//if its the same location
	{
		this->_board[From[0]][From[1]] = Tomove;
		this->_board[To[0]][To[1]] = Moveto;
		return "7";
	}
	else if (checkChess(!this->turn))//if a chess is made because of the move
	{
		this->turn = !this->turn;
		return "1";
	}
	this->turn = !this->turn;//if the move is valid
	return "0";
}
//this function gets a location(like c5) and returns the piece that in this location
//returns 0 if its out of the index of the board or if its empty
Piece* Board::getPieceInLocation(std::string location)
{
	try {
		char letter = location[0] - 97;
		char number = location[1] - 48 - 8;
		return this->_board[abs(int(number))][int(letter)];
	}
	catch(std::string error)
	{
		return nullptr;
	}
}
/*this function gets a location and moves the pieces on the board by the location*/
void Board::movePieces(int From[],int To[],std::string location)
{
	std::string firstLocation = location.substr(0, 2);
	std::string secondLocation = location.substr(2);
	Piece* Tomove = getPieceInLocation(firstLocation);
	
	_board[From[0]][From[1]] = 0;
	_board[To[0]][To[1]] = Tomove;
}
//this function gets a turn and checks if there is a chess on this color
//returns 1 if there is a chess,0 if not
int Board::checkChess(int turn)
{
	std::string location = "";
	int i = 0;
	int j = 0;
	int pos[2] = { 0,0 };
	int From[2] = { 0,0 };
	bool isKing = false;
	for (i = 0; i < 8; i++) {//checks for the king in the board
		for (j = 0; j < 8; j++) {
			if (_board[i][j]) {//find the needed king by the side 
				if (this->_board[i][j]->getColor() == turn && (_board[i][j]->getType() == 'k' || _board[i][j]->getType() == 'K')) {
					pos[0] = i;
					pos[1] = j;
					isKing = true;
					break;
				}
			}
		}
	}

	if (isKing) {

		for (i = 0; i < 8; i++) {//checks if each enemy player can eat the king
			for (j = 0; j < 8; j++) {//go threw every piece of the enemy and check if they made a chess on us
				if (_board[i][j]) {
					if (_board[i][j]->getColor() != turn) {
						From[0] = i;
						From[1] = j;
						if (_board[i][j]->checkMove(From, pos, _board) == 1) {
							return true;
						}
					}
				}
			}
		}
	}
	return false;
}