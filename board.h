#pragma once
#include <string.h>
#include <iostream>
#include "Piece.h"
#include "king.h"
#include "rook.h"
#include "bishop.h"
#include "queen.h"
#include "knight.h"
#include "pawn.h"

class Board
{
private:
	Piece* _board[8][8];
	int turn;
public:
	Board(std::string data);
	std::string MakeMove(std::string location);
	Piece* getPieceInLocation(std::string locations);//return which peace has that location
	void movePieces(int From[], int To[], std::string location);
	int checkChess(int turn);//1- there is a chess,0 there is no chess
	std::string getLocation(int i, int j, int i1, int j1);
};