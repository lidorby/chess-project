#include "knight.h"

Knight::Knight(int color) : Piece(color)
{
}

int Knight::checkMove(int From[2], int To[2], Piece * board[][8]) const
{
	if ((abs(From[0] - To[0]) == 2 && abs(From[1] - To[1]) == 1) || (abs(From[0] - To[0]) == 1 && abs(From[1] - To[1]) == 2))
	{
		return 1;
	}
	return 0;
}

char Knight::getType() const
{
	if (this->color)
	{
		return 'n';
	}
	return 'N';
}
