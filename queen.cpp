#include "queen.h"

Queen::Queen(int color) : Piece(color)
{
}

int Queen::checkMove(int From[2], int To[2], Piece * board[][8]) const
{
	Piece* rook = new Rook(color);
	Piece* bishop = new Bishop(color);
	if (rook->checkMove(From, To, board) || bishop->checkMove(From, To, board))
	{
		return 1;
	}
	return 0;
}

char Queen::getType() const
{
	if (this->color)
	{
		return 'q';
	}
	return 'Q';
}
